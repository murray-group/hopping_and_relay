#ifndef Points_H
#define Points_H

#include <vector>
#include <algorithm>
#include <set>

#include "ziggurat_inline.hpp"


#include "Ziggurat.h"

typedef double floating_point;

class Random_Number_Engine
{
public:

    //static int seed;
    static void init(int seed)
    {
        zigset ( 123456789*seed, 234567891*seed, 345678912*seed, 456789123*seed );
    }

    static void random_uni_vec(std::vector<floating_point>& unirands)
    {
        r4_uni_value_vec(unirands);
    }

    static void random_norm_vec(std::vector<floating_point>& normrands)
    {
        r4_nor_value_vec(normrands);
    }

    static void random_exp_vec(std::vector<floating_point>& exprands)
    {
        r4_exp_value_vec(exprands);
    }

    static floating_point random_exp()
    {
        return r4_exp_value ( );
    }

    static floating_point random_uni()
    {
        return r4_uni_value ( );
    }
};

struct Reaction {
    floating_point timing;
    int index;
    Reaction() : index(-1), timing(0)
    {
    }

    Reaction(int index,floating_point timing) : index(index), timing(timing)
    {
    }
} ;

bool operator <(const Reaction& x, const Reaction& y) {
    return x.timing < y.timing || (x.timing == y.timing && x.index > y.index); //addd or to ensure that set insert is working properly. other wise elements with the same timing are assumed to be equal
}
bool operator <=(const Reaction& x, const Reaction& y) {
    return x.timing <= y.timing;
}
bool operator >(const Reaction& x, const Reaction& y) {
    return x.timing > y.timing || (x.timing == y.timing && x.index > y.index); //addd or to ensure that set insert is working properly. other wise elements with the same timing are assumed to be equal
}
bool operator >=(const Reaction& x, const Reaction& y) {
    return x.timing >= y.timing;
}
bool operator ==(const Reaction& x, const Reaction& y) {
    return x.timing == y.timing && x.index == y.index;
}

//an instances of this class handles the occurence of reactions.
//reactions can be added and removed and it is possible the check which reactions happened given a certain time.
//each Reaction_List is only responsible for one kind of reaction.
class Reaction_List {
public:
    std::set<Reaction> reactions;
    floating_point tau;

    Reaction_List(floating_point tau):tau(tau)
    {
    }

    //adds a reaction to the set
    void add_reaction(int index, floating_point t)
    {
        Reaction r{index,t+Random_Number_Engine::random_exp()*tau};
        reactions.insert(r);
    }

    /*
    //adds a reaction to the set with a custom tau (this one is currently only for cooperativity of ParA
    void add_reaction(int index, floating_point t, floating_point tau)
    {
        Reaction r{index,t+Random_Number_Engine::random_exp()*tau};
        reactions.insert(r);
    }
    */

    //returns the index of a reaction (first) that occured, if no reaction occured returns -1
    int did_a_reaction_occur(floating_point t)
    {
        if(reactions.size() == 0)
        {
            return -1;
        }

        auto it = reactions.begin();
        if(it->timing <= t)
        {
            //int index = it->index;
            //std::cout << it->index << "stesttest \n";
            return it->index;
        }
        return -1;
    }

    //removes all elements of the set which have the specified index and reduces the index of all elements with a greater index by one
    //this is done by creating a new set and adding elements after the aforementioned criteria
    void remove_reaction(int index)
    {
        std::set<Reaction> reactions_new;

        for(auto it = reactions.begin(); it != reactions.end(); ++it)
        {
            if(it->index > index)
            {
                reactions_new.insert(Reaction(it->index-1,it->timing));
            }
            else if(it->index < index)
            {
                reactions_new.insert(Reaction(it->index,it->timing));
            }
        }
        reactions = reactions_new;
    }

    //update/changes the time a reaction occurs for a specific reation
    void update_reation(int index, floating_point t)
    {
        Reaction r{index,t+Random_Number_Engine::random_exp()*tau};
        std::set<Reaction> reactions_new;

        for(auto it = reactions.begin(); it != reactions.end(); ++it)
        {
            if(it->index != index)
            {
                reactions_new.insert(Reaction(it->index,it->timing));
            } else {
                reactions_new.insert(r);
            }
        }
        reactions = reactions_new;
    }
};


// class which stores one (x,y)-coordinate
class Point
{
public:
    floating_point x, y;

    // constructs a Point-object (default constructor)
    Point(): x(0.0),y(0.0) {};

    // constructs a Point-object and directly sets x and y
    Point(floating_point x,floating_point y):x(x),y(y) {};


};

Point operator+(const Point &A, const Point &B)//add points, returns new point
{
    Point result(A.x+B.x,A.y+B.y);
    return result;
}

Point operator*(const Point &A, const floating_point f)//add points, returns new point
{
    Point result(A.x*f,A.y*f);
    return result;
}

Point operator*(const floating_point f, const Point &B)//add points, returns new point
{
    Point result(B.x*f,B.y*f);
    return result;
}

Point operator+(const Point &A, const floating_point s)//add points, returns new point
{
    Point result(A.x+s,A.y+s);
    return result;
}

Point operator+(const floating_point s, const Point &B)//add points, returns new point
{
    Point result(B.x+s,B.y+s);
    return result;
}

Point & operator+=(Point &A, const Point &B)//+= add points, returns by reference
{
    A.x+=B.x;
    A.y+=B.y;
    return A;
}

Point operator-(const Point &A, const Point &B)//add points, returns new point
{
    Point result(A.x-B.x,A.y-B.y);
    return result;
}

Point & operator-=(Point &A, const Point &B)//+= add points, returns by reference
{
    A.x-=B.x;
    A.y-=B.y;
    return A;
}

template <typename T>
std::vector<T> operator+(const std::vector<T> &A, const std::vector<T> &B)//add vectors, returns new vector
{
    assert(A.size()==B.size());

    std::vector<T> result;
    result.reserve( A.size());                // preallocate memory

    std::transform(A.begin(), A.end(), B.begin(), std::back_inserter(result), std::plus<T>());
    return result;
}

template <typename T>
std::vector<T> & operator+=(std::vector<T> &A, const std::vector<T> &B)//+= add vectors, returns by reference
{
    assert(A.size()==B.size());

    std::transform(A.begin(), A.end(), B.begin(), A.begin(), std::plus<T>());
    return A;
}

template <typename T>
std::vector<T> operator+(const std::vector<T> &A, T &B)//add to every element, returns new vector
{
    std::vector<T> AB;
    AB.reserve( A.size());                // preallocate memory
    AB=A;
    for(T& d : AB)
    {
        d += B;
    }
    return AB;
}

template <typename T>
std::vector<T> & operator+=(std::vector<T> &A, T &B)//+= add to every element, returns by reference
{
    for(T& d : A)
    {
        d += B;
    }
    return A;
}




//// class which stores multiple (x,y)-coordinates in one vector
//class Positions
//{
//
//
//private:
//    //std::vector<floating_point> x_pos,y_pos;//for later summing over x or y positions, storing contiguously instead of as Point objects is better
//    std::vector<Point> pos;
//
//public:
//
//// constructs a Positions-object with the specified length
//    Positions(int n_points):x_pos(n_points),y_pos(n_points){};
//
//    // return one point with the specified index
//    Point get_point(int index)
//    {
//        return Point(x_pos[index],y_pos[index]);
//    };
//
//    size_t size()
//    {
//        return x_pos.size();
//    };
//
//    //adding a Point
//    void emplace_back(Point p)
//    {
//        x_pos.emplace_back(p.x);
//        y_pos.emplace_back(p.y);
//    };
//
//    //remove a Point
//    void erase(size_t i)
//    {
//        x_pos.erase(x_pos.begin()+i);
//        y_pos.erase(y_pos.begin()+i);
//    };
//
//
//};




#endif
