#include <iostream>
#include <unistd.h>
#include <memory>
#include <fstream>
#include <sstream>
#include "data_structures.h"
#include "ParA.h"


//using namespace std;
typedef double floating_point;

void primitive_visualization(std::vector<std::vector<Point>> & posA, std::vector<Point> & posPC, Domain & dom, int buckets_x,int buckets_y);


//int main()
int main(int argc, char *argv[])
{
    std::cout<<"Starting main\n";

    //--------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------Parameters--------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------

    //Time, Number of particles, Fluctuations
    floating_point t_end=3600.0;//end time
    floating_point t_init=0;//time before the simulations starts to eqluibirate the ParA (does not count as addition time and has no impact on e.g. dormant timings)
    floating_point dt=0.001;
    int n_pc = 1;
    int n_pa = 400;
    int n_pb = n_pa;//number of ParB on each plasmid (no more than this tethers can be attached to a plasmid at each point in time
    floating_point sigma_x = 0.1; //Standard deviations of fluctuating DNA-bound ParA
    floating_point sigma_y = 0.05;
    floating_point p_n = 1000; //Potential nucleoid (how fast ParA move up the stepest nucleoid gradient)

    //Domain
    floating_point w0 = 2;
    floating_point l0 = 0.5;

    //PC
    floating_point tau_off = 1/0.1;
    floating_point R = 0.05 + 0.002;
    floating_point D_P = 0.003;

    //ParA
    floating_point D_A = 0.01; //diffusion ParA
    floating_point D_H = 0.0; //diffusion home position on nucleoid
    floating_point tau_a = 1/0.1; //nucleoid binding rate
    floating_point tau_d = 1/0.00000000001; //nucleoid unbinding rate
    floating_point tau_c0 = 1/0.00000000001; //ParA cooperativity
    floating_point growth_factor = 0; //The amout by which the cell growth per time step (at the end of interpreting parameters it will be converted into growth factor)
    bool growth = false;

    //capture time
    floating_point capture_every = 10; //Capture every (seconds)
    floating_point capture_start = 0; //Start capturing (seconds)
    bool save_ParA_positions = false;

    //random start
    bool random_start = false;
    bool random_rep = true;

    //defined start positions
    std::vector<floating_point> defined_positions;

    //dormant stuff
    bool dormant = false;
    int inital_pc = 1;
    int current_dormant_state = inital_pc;
    std::vector<floating_point> dormant_timings;
    floating_point dormant_next_change = 0;

    bool increase_dormant = true;

    //the user can provided a matrix of the nucleoid to influence the movement of the ParA
    bool nucleoid = false;
    std::vector<std::string> nucleoid_paths;
    std::vector<floating_point> nucleoid_timings;

    bool debug = false;
    std::string parameters_file;


    //choose display intervalls
    floating_point display_every = t_end+1;

    std::string run = "0";
    std::string task_id = "0";

    //parse arguments
    std::string base_path = argv[1];

    //there are two types of arguments (1) flags which start with the character '-' (2) parameters which are values/paths or any other kind information
    std::vector<std::vector<std::string>> para;
    std::vector<std::string> current_para;
    for(int i = 2; i < argc; i++) {//parse all arguments supplied to this script

        current_para.push_back(argv[i]);
        if(i == argc-1 || argv[i+1][0]=='-'){//if the next parameter starts with '-' or if there are no next parameters add the current flag + parameters to the para list
            if(current_para[0]!="-parameters_file"){
                para.push_back(current_para);
            } else {
                std::ifstream in_file;
                in_file.open(current_para[1]);
                current_para.clear();
                std::string s;
                while (in_file >> s) {
                    if(s[0] == '-') {
                        if(!current_para.empty()){
                            para.push_back(current_para);
                        }
                        current_para.clear();
                        current_para.push_back(s);
                    } else {
                        current_para.push_back(s);
                    }
                }
                para.push_back(current_para);
            }
            current_para.clear();
        }
    }


    //print all the supplied parameters
    for(size_t i = 0; i<para.size(); i++){
        std::vector<std::string> current_para;
        current_para = para[i];
        for(size_t j = 0; j<current_para.size(); j++){
            std::cout << current_para[j] << " ";
        }
        std::cout << "\n";
    }


    //interpret parameters
    for(size_t i = 0; i < para.size(); i++){
        std::vector<std::string> current_para = para[i];
        if(current_para[0] == "-run") {
            run = current_para[1];
        } else if(current_para[0] == "-task") {
            task_id = current_para[1];
        } else if(current_para[0] == "-t_end") {
            t_end = std::stod(current_para[1]);
        } else if(current_para[0] == "-t_init") {
            t_init = std::stod(current_para[1]);
        } else if(current_para[0] == "-dt") {
            dt = std::stod(current_para[1]);
        } else if(current_para[0] == "-n_pc") {
            n_pc = std::stoi(current_para[1]);
        } else if(current_para[0] == "-n_pa") {
            n_pa = std::stoi(current_para[1]);
        } else if(current_para[0] == "-n_pb") {
            n_pb = std::stoi(current_para[1]);
        } else if(current_para[0] == "-sigma_x") {
            sigma_x = std::stod(current_para[1]);
        } else if(current_para[0] == "-sigma_y") {
            sigma_y = std::stod(current_para[1]);
        } else if(current_para[0] == "-w0") {
            w0 = std::stod(current_para[1]);
        } else if(current_para[0] == "-l0") {
            l0 = std::stod(current_para[1]);
        } else if(current_para[0] == "-R") {
            R = std::stod(current_para[1]);
        } else if(current_para[0] == "-D_P") {
            D_P = std::stod(current_para[1]);
        } else if(current_para[0] == "-D_A") {
            D_A = std::stod(current_para[1]);
        } else if(current_para[0] == "-D_H") {
            D_H = std::stod(current_para[1]);
        } else if(current_para[0] == "-k_a") {
            tau_a = 1/std::stod(current_para[1]);
        } else if(current_para[0] == "-k_d") {
            tau_d = 1/std::stod(current_para[1]);
        } else if(current_para[0] == "-k_c") {
            tau_c0 = 1/std::stod(current_para[1]);
        } else if(current_para[0] == "-k_off") {
            tau_off = 1/std::stod(current_para[1]);
        } else if(current_para[0] == "-k_g") {//growth rate
            growth_factor = std::stod(current_para[1]);//at the end growth rate will be converted into growth factor
            growth = true;
        } else if(current_para[0] == "-p_n") {
            p_n = std::stod(current_para[1]);
        } else if(current_para[0] == "-p_s") { //define starting positions of plasmids
            for(size_t j = 1; j < current_para.size(); j++){
                defined_positions.push_back(std::stod(current_para[j]));
            }
        } else if(current_para[0] == "-c_e") {
            capture_every = std::stod(current_para[1]);
        } else if(current_para[0] == "-c_s") {
            capture_start = std::stod(current_para[1]);
        } else if(current_para[0] == "-r_s") {
            random_start = true;
        } else if(current_para[0] == "-s_ParA") {
            save_ParA_positions = true;
        } else if(current_para[0] == "-dormant") {//-flag number_dormat_at_beginning timinng1 timing2 ...
            dormant = true;
            inital_pc = std::stoi(current_para[1]);
            current_dormant_state = inital_pc;
            for(size_t j = 2; j < current_para.size(); j++){
                dormant_timings.push_back(std::stod(current_para[j]));
            }
        } else if(current_para[0] == "-nucleoid") {//-flag path1 timing1 path2 timing2 ....
            nucleoid = true;
            for(size_t j = 1; j < current_para.size(); j+=2){
                nucleoid_paths.push_back(current_para[j]);
            }
            for(size_t j = 2; j < current_para.size(); j+=2){
                nucleoid_timings.push_back(std::stod(current_para[j]));
            }
        } else if(current_para[0] == "-non_random_rep") {//biased replication based on how many plasmids are in each cell half
            random_rep = false;
        } else if(current_para[0] == "-show") {
            display_every = std::stod(current_para[1]);
        } else if(current_para[0] == "-debug") {
            debug = true;
        } else {
            std::cout << "Unrecognised parameter: " << current_para[0] << "\n";
        }
    }
    growth_factor = exp(dt*growth_factor);
    std::cout << "\n";
    std::cout << "\n";
    std::cout << "\n";

    //output paths
    long int current_time = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch()).count();
    std::string output_path = base_path + "output_" + std::string(5 - task_id.length(), '0') + task_id + "_" + std::to_string(current_time) + ".csv";
    std::string parameters_path = base_path + "parameters_" + std::string(5 - task_id.length(), '0') + task_id + "_" + std::to_string(current_time) + ".txt";
    std::cout << "Path data: " <<output_path << "\n";
    std::cout << "Path parameters: " <<output_path << "\n";
    std::cout << "\n";
    std::cout << "\n";
    std::cout << "\n";

    if(debug)
        std::cout<<"Saving parameters\n";

    //save parameters
    std::ofstream parameters_writer;
    parameters_writer.open(parameters_path);
    for(size_t i = 0; i < para.size(); i++){
        std::vector<std::string> current_para = para[i];
        for(size_t j = 0; j < current_para.size(); j++){
            parameters_writer << current_para[j];
            if(j!=current_para.size()){
                parameters_writer <<  " ";
            }
        }
        if(i!=para.size()-1){
            parameters_writer << "\n";
        }
    }
    parameters_writer.close();

    //--------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------Initiation--------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------

    //init Random_Number_Engine
    if(debug)
        std::cout<<"Initiate random number engine\n";

    Random_Number_Engine::init(std::chrono::system_clock::now().time_since_epoch().count());
    size_t seed=std::chrono::system_clock::now().time_since_epoch().count();
    Ziggurat::Ziggurat z(seed);


    //init Domain
    if(debug)
        std::cout<<"Initiate domain\n";
    Domain dom{w0,l0};


    //init PC
    if(debug)
        std::cout<<"Initiate partition complexes\n";
    std::vector<std::unique_ptr<PC>> pc(n_pc);
    for (auto& p : pc){
        p = std::make_unique<PC>(dom, dt, D_P, n_pb, tau_off, R, sigma_x, sigma_y,false);
        if(random_start) { //otherwise they started all at the same location: (pom.Lx/5,dom.Ly/2)
            //start at random position
            std::vector<floating_point> unirands(2);
            Random_Number_Engine::random_uni_vec(unirands);
            Point pos(0,0);
            pos.x = unirands[0]*dom.Lx;
            pos.y = unirands[1]*dom.Ly;
            //pos.x = dom.Lx*8/9;
            //pos.y = dom.Ly/2;
            p->pos = pos;
        }
    }


    //put plasmids at user defined positions x positions (long axis) the y position is half the nucleoid (overwrites random start)
    for (int i = 0; i < defined_positions.size() && i < n_pc; i++) {
        pc[i]->pos.x = defined_positions[i];
        pc[i]->pos.y = dom.Ly/2;
    }


    //init nucleoid density
    std::vector< std::vector< std::vector<floating_point> > > nucleoid_densities;
    floating_point nucleoid_next_change = 0;
    if(nucleoid){
        if(debug)
            std::cout<<"Initiate nucleoid density\n";
        for (int i = 0; i < nucleoid_paths.size(); i++) {

            std::vector<std::vector<floating_point> > nucleoid_density;
            std::string s;
            std::string entry;
            std::ifstream in_file;
            in_file.open(nucleoid_paths[i]);

            while (in_file >> s) {
                std::vector<floating_point> nucleoid_density_slice;
                std::stringstream ss(s);
                for(int x=0; ss.good();x++)
                {
                    getline( ss, entry, ',' );
                    double density = std::stod(entry);
                    nucleoid_density_slice.push_back(density);
                    //std::cout << entry << " ";
                }
                //std::cout << std::endl << "-----------------------------" << std::endl;
                nucleoid_density.push_back(nucleoid_density_slice);
            }
            in_file.close();
            nucleoid_densities.push_back(nucleoid_density);
        }

        dom.set_nucleoid_density(nucleoid_densities.front());
        nucleoid_densities.erase(nucleoid_densities.begin());

        if(nucleoid_timings.empty())
        {
            nucleoid_next_change = t_end+1;
        }
        else
        {
            nucleoid_next_change = nucleoid_timings.front();
            nucleoid_timings.erase(nucleoid_timings.begin());
        }
    }

    //initiate dormant plasmids
    if(dormant)
    {
        if(debug)
            std::cout<<"Initiate dormant plasmids\n";
        for(int i = inital_pc; i < pc.size(); i++)//set pc to be dormant except the first
        {
            pc[i]->dormant=true;
        }

        if (dormant_timings.empty())//set the first timing when dormant state changes
        {
            dormant_next_change = t_end+1;
        }
        else
        {
            dormant_next_change = dormant_timings.front();
            dormant_timings.erase(dormant_timings.begin());
        }
    }


    //init ParA
    if(debug)
        std::cout<<"Initiate ParA\n";
    ParA pa{n_pa,dom,dt,D_A,D_H,tau_a,tau_d,tau_c0,sigma_x,sigma_y,p_n};

    double t=-t_init;
    floating_point last_display = 0;
    floating_point last_capture = -capture_every;
    floating_point last_dormant = 0;
    floating_point last_nucleoid = 0;



    if(debug)
        std::cout<<"Initiate output writer\n";
    std::ofstream output_writer;
    output_writer.open(output_path);
    int n_add2cyto;
    int n_plasmid_unbinding_events = 0;
    int n_nuc_unbinding_events = 0;


    //------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------MainLoop--------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------------------------------------------------

    if(debug)
        std::cout<<"Starting simulations\n";
    while(t<=t_end){
        //Growth of the cell
        if(growth && t>=0){
            dom.grow(growth_factor);
            pa.offset_x_due_to_growth(growth_factor);
            for(size_t i = 0; i < pc.size(); i++)
            {
                pc[i]->offset_x_due_to_growth(growth_factor);
            }
        }

        //Reactions
        //--ParA
        n_nuc_unbinding_events += pa.nuc_binding_unbinding_ParA(t);
        //--PC
        n_add2cyto = 0;
        for(size_t i = 0; i < n_pc; i++)
        {
            n_add2cyto += pc[i]->tethers_breaking(t);
        }
        n_plasmid_unbinding_events += n_add2cyto;
        pa.add_cyto_ParAs(n_add2cyto,t);

        //Moves
        //--ParA move
        pa.diffusive_and_elastic_move();
        //--PC move
        if(t>=0) {
            for(size_t i = 0; i < n_pc; i++)
            {
                pc[i]->complex_move();
            }
        }

        //ParA-PC binding events
        for(size_t i = 0; i < n_pc; i++)
        {
            pa.binding_events(pc[i],t);
        }

        //change nucleoid density if it is time to do so (specified through parameters)
        if(last_nucleoid+nucleoid_next_change<=t && !nucleoid_densities.empty())
        {
            if (debug)
                std::cout<<"Nucleoid density change at "<<t<<"s\n";

            last_nucleoid += nucleoid_next_change;
            dom.set_nucleoid_density(nucleoid_densities.front());
            nucleoid_densities.erase(nucleoid_densities.begin());

            if(nucleoid_timings.empty())
            {
                nucleoid_next_change = t_end+1;
            }
            else
            {
                nucleoid_next_change = nucleoid_timings.front();
                nucleoid_timings.erase(nucleoid_timings.begin());
            }

            if (debug)
                std::cout<<"Nucleoid density was changed\n";
        }

        //change dormant state
        if(last_dormant+dormant_next_change<=t && dormant)
        {
            if (debug)
                std::cout<<"Plasmid changes dormant state at "<<t<<"s\n";
            //std::cout <<dormant_next_change << "\n";
            if(current_dormant_state==n_pc)
                increase_dormant = false;

            if(current_dormant_state == 0){
                increase_dormant = true;
            }

            //std::cout <<increase_dormant << "\n";
            //std::cout <<current_dormant_state << "\n";
            //std::cout <<t << "\n";

            Point spawn_pos(0,0);
            if(increase_dormant)//handle if the number of plasmids increase or decrease
            {
                pc[current_dormant_state]->dormant=false;

                std::vector<int> left;
                std::vector<int> right;


                for(int i = 0; i < current_dormant_state; i++){//check how many plasmids are on each side of the cell
                    if(pc[i]->pos.x<dom.Lx/2){
                        left.push_back(i);
                    } else {
                        right.push_back(i);
                    }
                }
                int n_left = static_cast<int>(left.size());
                int n_right = static_cast<int>(right.size());

                if(!random_rep && !(n_left == 0 || n_right == 0 || n_left == n_right)) {//biased replication, replication is likely to ocour on the side with fewer plasmids

                    std::vector<floating_point> unirands(2);
                    Random_Number_Engine::random_uni_vec(unirands);
                    floating_point prob = static_cast<double>(std::abs(n_left - n_right));
                    prob = (prob+1)/(prob+2);//probablility that replication ocours on the side with fewer plasmids

                    std::vector<int> tmp;
                    if(unirands[0]<=prob){
                        if(n_left<n_right){
                            tmp = left;
                        } else {
                            tmp = right;
                        }
                    } else {
                        if(n_left<n_right){
                            tmp = right;
                        } else {
                            tmp = left;
                        }
                    }
                    spawn_pos = pc[tmp[static_cast<int>(unirands[0]*static_cast<floating_point>(tmp.size()))]]->pos;// pick one plasmid and rep.


                } else {//this is just random replication
                    if(current_dormant_state == 0)
                    {
                        std::vector<floating_point> unirands(2);
                        Random_Number_Engine::random_uni_vec(unirands);
                        spawn_pos.x = unirands[0]*dom.Lx;
                        spawn_pos.y = unirands[1]*dom.Ly;
                    }
                    else
                    {
                        std::vector<floating_point> unirands(1);
                        Random_Number_Engine::random_uni_vec(unirands);
                        spawn_pos = pc[static_cast<int>(unirands[0]*current_dormant_state)]->pos;
                    }
                }
                pc[current_dormant_state]->pos=spawn_pos;
                current_dormant_state++;
            } else {
                //std::cout <<current_dormant_state<< "\n";
                pc[current_dormant_state-1]->dormant=true;
                current_dormant_state--;
            }

            last_dormant = last_dormant+dormant_next_change;//update when the dormant state changed

            if (dormant_timings.empty())//update when the next change in dormant changes
            {
                dormant_next_change = t_end+100000000000000;
            }
            else
            {
                dormant_next_change = dormant_timings.front();
                dormant_timings.erase(dormant_timings.begin());
            }
            //std::cout <<"\n\n";
            if (debug)
                std::cout<<"Plasmid changed dormant state\n";
        }

        //save state
        if(last_capture+capture_every<=t&&capture_start<=t)
        {
            if (debug)
                std::cout<<"Saving data at "<<t<<"s\n";
            std::string output_row = "";
            //there are 5 fields at the beginning which can be used to save additional information
            //to see how much ATP is hydrolized
            //output_writer << std::to_string(n_nuc_unbinding_events) << "," << std::to_string(n_plasmid_unbinding_events) << ",";//first two are filled with nucleoid binding and unbinding events
            //output_row += std::to_string(n_nuc_unbinding_events) + "," + std::to_string(n_plasmid_unbinding_events) + ",";
            //output_writer << ",,,";//The over three are empty so far
            output_row += std::to_string(dom.Lx);
            output_row += ",,,,,";
            //output_row += ",,,,,";
            n_nuc_unbinding_events = 0;
            n_plasmid_unbinding_events = 0;

            for(int i = 0; i < pc.size(); i++)
            {
                //output_writer << std::to_string(pc[i]->pos.x) << "," << std::to_string(pc[i]->pos.y) << ",";
                output_row += std::to_string(pc[i]->pos.x) + "," + std::to_string(pc[i]->pos.y) + ",";
                if(pc[i]->dormant)
                {
                    //output_writer << "0,";
                    output_row += "0,";
                } else {
                    //output_writer << "1,";
                    output_row += "1,";
                }
                //output_writer << ",,";//there are two additional fields after is_dormat value to store additional information
                output_row += ",,";
            }


            if(save_ParA_positions)
            {
                for(int i = 0; i < pa.curr_pos.size(); i++)
                {
                    //output_writer << std::to_string(pa.curr_pos[i].x) << "," << std::to_string(pa.curr_pos[i].y) << ",-1,";
                    output_row += std::to_string(pa.curr_pos[i].x) + "," + std::to_string(pa.curr_pos[i].y) + ",-1,";
                }


                for(int j = 0; j < pc.size(); j++)
                {
                    for(int i = 0; i < pc[j]->curr_pos.size(); i++)
                    {
                        //output_writer << std::to_string(pc[j]->curr_pos[i].x) << "," << std::to_string(pc[j]->curr_pos[i].y) << "," << std::to_string(j) << ",";
                        output_row += std::to_string(pc[j]->curr_pos[i].x) + "," + std::to_string(pc[j]->curr_pos[i].y) + "," + std::to_string(j) + ",";
                    }
                }
            }
            output_row = output_row.substr(0, output_row.size()-1);
            output_writer << output_row << "\n";
            while(last_capture+capture_every<=t)
            {
                last_capture = last_capture+capture_every;
            }
            if (debug)
                std::cout<<"Data was saved\n";
        }

        //display state
        int n_pa_proof = 0;//to check if there are any ParA which disapear
        if(last_display+display_every<=t)
        {
            std::vector<std::vector<Point>> posA(n_pc+1);
            std::vector<Point> posPC(n_pc);
            posA[0] = pa.home_pos;
            n_pa_proof += pa.curr_pos.size();
            n_pa_proof += pa.nuc_binding.reactions.size();
            for(size_t i = 0; i < pc.size(); i++)
            {
                posA[i+1] = pc[i]->home_pos;
                posPC[i] = pc[i]->pos;
                n_pa_proof += pc[i]->curr_pos.size();
            }

            std::cout << "current time: \t"<< t << "s\n";
            std::cout << "Domain Length: \t" << dom.Lx << std::endl;
            primitive_visualization(posA,posPC,dom,75,8);
            //usleep(1000000);
            last_display += display_every;
        }

        //update time
        t+=dt;
    }
    if (debug)
        std::cout<<"Simulation finsihed\n";

    if (debug)
        std::cout<<"Closing output writer\n";
    output_writer.close();

    if (debug)
        std::cout<<"Finished!\n";
    return 0;
}

//visualizes all parA in its different states
void primitive_visualization(std::vector<std::vector<Point>> &posA, std::vector<Point> & posPC, Domain &dom, int buckets_x,int buckets_y)
{
    std::vector<std::vector<int>> canvas(buckets_y);
    std::vector<std::vector<int>> number(buckets_y);
    for(auto i = 0; i < buckets_y; i++)
    {
        //std::vector<int> tmp;
        canvas[i].resize(buckets_x);
        number[i].resize(buckets_x);
    }
    int x, y;
    for(size_t j=0; j<posA.size(); j++)
    {
        std::vector<Point> pos = posA[j];
        for(size_t i=0; i<pos.size(); i++)
        {
            x = (int)(pos[i].x/dom.Lx*buckets_x);
            y = (int)(pos[i].y/dom.Ly*buckets_y);
            if(x>=buckets_x)
                x = buckets_x -1;
            if(x < 0)
                x = 0;
            if(y>=buckets_y)
                y = buckets_y -1;
            if(y < 0)
                y = 0;
            //int a = state[i]+1;
            canvas[y][x] = j+1;
            number[y][x] = number[y][x]+1;
        }
    }

    for(size_t j=0; j<posPC.size(); j++)
    {
        x = (int)(posPC[j].x/dom.Lx*buckets_x);
        y = (int)(posPC[j].y/dom.Ly*buckets_y);
        canvas[y][x] = -j-2;
    }

    for(auto i = 0; i < buckets_y; i++)
    {
        for(auto j = 0; j < buckets_x; j++)
        {
            if(canvas[i][j]>9 || canvas[i][j]<-9){
                std::cout << "\033[1;37;40mX\033[0m";
                //std::cout << "X";
            }
            else if(canvas[i][j]==1){
                int r, g, b;
                switch (number[i][j]) {
                    case 1:
                        r = g = b = 60;
                        break;
                    case 2:
                        r = g = b = 80;
                        break;
                    case 3:
                        r = g = b = 100;
                        break;
                    case 4:
                        r = g = b = 140;
                        break;
                    case 5:
                        r = g = b = 180;
                        break;
                    case 6:
                        r = g = b = 200;
                        break;
                    case 7:
                        r = g = b = 220;
                        break;
                    default:
                        r = g = b = 240;
                        break;
                }
                std::cout << "\033[48;2;" << r << ";" << g << ";" << b << "m \033[0m";
            }
            else if(canvas[i][j]==0)
            {
                //std::cout << "-";
                std::cout << "\033[1;40m \033[0m";
            }
            else if(canvas[i][j]<0)
            {
                std::cout << "\033[1;37;" << 40-canvas[i][j]-1 << "m*\033[0m";
            }
            else
            {
                std::cout << "\033[1;37;" << 40+canvas[i][j]-1 << "m \033[0m";
            }

        }
        std::cout << "\n";
    }
    std::cout << "\n";
    std::cout << "\n";
    std::cout << "\n";
}
