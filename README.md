### Hopping and Relay

#### Description

The repository contains a computational model of the ParABS system. It is an extension of the previous DNA-relay model (Surovtsev IV, Campos M, Jacobs-Wagner C. 2016a. DNA-relay mechanism is sufficient to explain ParA-dependent intracellular transport and patterning of single and multiple cargos. Proc Natl Acad Sci E7268–E7276. doi:10.1073/pnas.1616118113) that incorporates a reference nucleoid, diffusion on the nucleoid (hopping), cell growth, basal hydrolysis and cooperative binding of ParA and uses analytic expressions for the fluctuations rather than a second order approximation. Like the DNA relay, it is a 2D off-lattice stochastic model and updates positions in discrete time steps (dt). The implementation was written in C++. It consists of the following components:

- ParA associates non-specifically with the DNA in its ATP-dependent dimer state with the rate ka or through cooperative binding with the rate k_c.
- Once associated, ParA (i.e., ParA-ATP dimers) moves in two distinct ways:
  (i) Diffusive motion on the nucleoid with the diffusion coefficient D_H and if a non-uniform nucleoid is specified ParA's motion being biased towards the highest DNA density.
  (ii) Between hopping events, each bound ParA dimer experiences the elastic fluctuations of the DNA strand it is bound to. This is implemented as elastic (spring-like) fluctuations around its initial position.
- Dimers dissociate from the nucleoid due to either basal ATP hydrolysis at a rate k_d or due to hydrolysis stimulated by ParB on the plasmid. The latter is modeled as a ParB-coated disc, and ParB-ParA tethers form whenever the disk comes in contact with a ParA dimer. ParB-stimulated hydrolysis then breaks these tethers at a rate k_off, returning ParA to the cytosolic pool.
- The plasmid experiences the elastic force of every tethered ParA and moves according to its intrinsic diffusion coefficient Dp and the resultant force of all tethers.

As in the DNA relay model, we have made some simplifications that we next make explicit:

- First, we only model three states of ParA: 'nucleoid associated' and 'cytosolic' and 'tethered'.
- Second, cytosolic ParA is assumed to be well mixed. This is justified based on the slow conformation changes needed to return it to a state competent for DNA binding (Vecchiarelli et al., 2010).
- Third, no individual ParB molecules were modeled; rather, the plasmid is treated as a disk coated with enough ParB that each nucleoid-bound ParA that makes contact with the plasmid instantaneously finds a ParB partner, therefore removing the need to model individual ParB. This is justified by the substantially higher local concentration of ParB compared to ParA at the plasmid.

The nucleoid is modeled as a rectangle with the dimensions w0 x l0.

#### Parameters

The model accepts the following parameters:

- `-run [string]` Part of the name of the output file (default: "0")
- `-task [string]` Part of the name of the output file (default: "0")
- `-t_end [float]` Runtime of the simulations (default: 3600.0 s)
- `-t_init [float]` Time run to equilibrate ParA. Only ParA dynamics (binding, unbinding, interacting with PC) take place. Plasmids are frozen in place, and the cell does not grow. Data are only saved after the initiation period (default: 0.0 s)
- `-dt [float]` Time steps of the simulation (default: 0.001 s)
- `-n_pc [integer]` Number of partition complexes (default: 1)
- `-n_pa [integer]` Number of ParA dimer (default: 400)
- `-n_pb [integer]` Number of ParA dimer (default: n_pa)
- `-sigma_x [float]` Standard deviations of fluctuating DNA-bound ParA along cell width (default: 0.1 um)
- `-sigma_y [float]` Standard deviations of fluctuating DNA-bound ParA along cell length (default: 0.05 um)
- `-w0 [float]` Cell width (default: 2 um)
- `-l0 [float]` Cell length (default: 0.5 um)
- `-R [float]` Radius of partition complex (default: 0.052 um)
- `-D_P [float]` Diffusion of partition complex (default: 0.003 um^2/s)
- `-D_A [float]` Diffusion of ParA (default: 0.01 um^2/s)
- `-D_H [float]` Diffusion of the home position of nucleoid-bound ParA (default: 0.0 um^2/s)
- `-k_a [float]` ParA-nucleoid association rate (default: 0.1 1/s)
- `-k_d [float]` ParA-nucleoid disassociation rate (default: 0.00000000001 1/s)
- `-k_c [float]` Cooperative ParA binding rate (default: 0.00000000001 1/s)
- `-k_off [float]` ParA hydrolysis rate at the partition complex (default: 0.1 1/(s*ParA))
- `-k_g [float]` Growth rate (default: 0 um/s)
- `-p_n [float]` Nucleoid effector, defines the bias of ParA to move up the nucleoid density (default: 0.0)
- `-p_s [float float float ...]` Defines the starting position of the Partition complexes along the long axis of the nucleoid (optional)
- `-c_e [float]` Capture every (default: 10.0 s)
- `-c_s [float]` Start capturing (default: 0.0 s)
- `-r_s` Partition complex starts at a random position
- `-s_ParA` Save the location of all nucleoid-bound ParA (optional)
- `-dormant [int float float ...]` The simulation starts with the number of plasmids specified by the first value. Each further specified value defines after what time (starting from the start of the simulations or after the last plasmid was replicated) a plasmid is replicated (optional)
- `-non_random_rep` biased replication based on the difference in number in each cell half (replication happens more likely in the cell half with less plasmids. 50:50 if balanced, 66:33 when difference is one, 75:25 for dif. of 2, 80:20 ...
- `-nucleoid [string float string float string float ...]` Strings define the paths to comma-separated files defining the nucleoid profile; float defines the time until the next specified nucleoid is used (optional)
- `-p_n [float]` Nucleoid effector, defines the bias of ParA to move up the nucleoid density (default 0.0)
- `-show [float]` Displays the state of the simulations in the terminal (default: t_end+1)
- `-debug` More verbose output in the terminal
- `-parameters_file [string]` Path to a parameters file that contains multiple parameters. Each simulation creates a parameters file that can be used to run subsequent simulations with the exact same parameters.

#### Execution

After compiling, the model can be run as follows:<br>
`./dna_relay /Path/To/Output/Directory/ task 42 run 69 -l0 0.9 -w0 2.6 -n_pc 5 -t_end 7200 -n_pa 500 -k_d 0.01 -D_H 0.05676 -k_off 1 -k_a 0.01 -D_P 0.003 -r_s filler -dormant 600 -c_e 60`

## File Creation

This action creates two files in the directory `/Path/To/Output/Directory/`, named as follows:

- `output_00043_timestamp.csv`
- `parameters_00043_timestamp.txt`

## Output

Running a simulation generates two types of files:

- `output_*.csv`: The first 5 columns contain additional information about the simulations (currently, only the first is used for cell length, which changes when a growth rate is specified; the other 4 are empty). Following this, there are 5 columns for each partition complex (the first two contain x and y positions, and the third indicates if the plasmid is active (1) or dormant (0). The last two are empty). If `s_ParA` (save ParA) is specified, each nucleoid-bound ParA has three columns (the first two contain x and y positions, followed by its binding partner (-1 for no partner, 0 for the first partition complex, 1 for the second partition complex, ...)).

- `parameters_*.txt`: Contains all the parameters that were used in the simulation.
