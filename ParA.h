#include "data_structures.h"
#include <chrono>
#include <random>
#include <math.h>       /* sqrt */
#include <memory>
#include "ziggurat_inline.hpp"

typedef double floating_point;

class Domain
{
public:

    floating_point Lx,Ly;
    std::vector<std::vector<floating_point>> nucleoid_density;
    bool has_nucleoid_density = false;
    floating_point total_nucleoid_density;
    floating_point nx,ny;


    Domain(floating_point x,floating_point y):Lx(x),Ly(y) {};

    inline void set_nucleoid_density(std::vector<std::vector<floating_point>> nd)
    {
        nx =  nd.size()-1;
        ny =  nd[0].size()-1;
        has_nucleoid_density = true;


        total_nucleoid_density = 0;
        for(int x = 0; x < nx; x++){
            for(int y = 0; y < ny; y++){
                total_nucleoid_density += nd[x][y];
            }
        }

        if(total_nucleoid_density==0){
            std::cout<<"Something went wrong with the nucleoid density profile";
            has_nucleoid_density = false;
        }
        nucleoid_density = nd;
        //std::cout<<cum_sum<<"\n";
    }

    //check if a given point p is inside of the domain and if not reflect it at the corresponding border/s
    inline void reflective_border(Point &p)
    {
        if(p.x < 0.0)
        {
            p.x *= -1;
        }
        else if(p.x > Lx)
        {
            p.x = 2*Lx - p.x;
        }

        if(p.y < 0.0)
        {
            p.y *= -1;
        }
        else if(p.y > Ly)
        {
            p.y = 2*Ly - p.y;
        }
    }

    //check if a given point p with a radius R is inside of the domain and if not reflect it at the corresponding border/s
    inline void reflective_border(Point &p, floating_point R)
    {
        if(p.x-R < 0.0)
        {
            p.x = 2*R-p.x;
        }
        else if(p.x > Lx-R)
        {
            p.x = 2*Lx - 2*R - p.x;
        }

        if(p.y-R < 0.0)
        {
            p.y = 2*R-p.y;
        }
        else if(p.y > Ly)
        {
            p.y = 2*Ly - 2*R - p.y;
        }
    }

    inline void grow(floating_point growth_factor)
    {
        Lx*=growth_factor;
        //std::cout << Lx << std::endl;
    }

    /*
    //if a nucleoid density is given reject moves based on density
    inline void scale_move(Point &p_new, Point &distance)
    {
        if(has_nucleoid_density)
        {
            floating_point density = get_density(p_new);
            if(density<0.05)
            {
                density = 0.05;
            }
            density = sqrt(density);
            distance.x *= 1/density;
            distance.y *= 1/density;
        }
    }
    */

    //return a random binding position on the nucleoid. if nucleoid is given make diecision based on density
    inline Point get_binding_position()//the speed of this function can probably be massively improved
    {
        /*
        std::vector<floating_point> unirands(2);
        Random_Number_Engine::random_uni_vec(unirands);
        Point p{Lx*unirands[0],Ly*unirands[1]};
        if(has_nucleoid_density) {
            while(true) {
                floating_point density = get_density(p);
                std::vector<floating_point> unirands2(3);
                Random_Number_Engine::random_uni_vec(unirands2);
                if(unirands[2]<density){
                    break;
                }
                p.x = Lx*unirands2[0];
                p.y = Ly*unirands2[1];
            }
        }
        */
        std::vector<floating_point> unirands(2);
        Random_Number_Engine::random_uni_vec(unirands);
        Point p{Lx*unirands[0],Ly*unirands[1]};
        floating_point cum_sum = 0;
        int dpx = 0;//discrete position x
        int dpy = 0;//discrete position y
        if(has_nucleoid_density) {
            for(int x = 0; x < nx; x++){
                for(int y = 0; y < ny; y++){
                    cum_sum += nucleoid_density[x][y]/total_nucleoid_density;
                    if(cum_sum>unirands[0]){
                        dpy = y;
                        break;
                    }
                }
                if(cum_sum>unirands[0]){
                    dpx = x;
                    break;
                }
            }

            p.x = dpx/nx*Lx;
            p.y = dpy/ny*Ly;
            //std::cout << p.x << "\n";
            //std::cout << p.y << "\n";
        }
        //if(cum_sum<=unirands[0]){
        //    std::cout << "error: " << cum_sum << " " << unirands[0] << "\n" << dpy << " " << dpx << "\n";
        //}
        return p;
    }

    inline floating_point get_density(Point &p)
    {
        return nucleoid_density[(int)(p.x/Lx*nx)][(int)(p.y/Ly*ny)];
    }

    inline Point get_derivative(Point &p){
        //std::cout << p.x << " " << p.y << "\n\n";
        floating_point tx = p.x/Lx*nx;
        floating_point ty = p.y/Ly*ny;
        floating_point x = tx-(int)tx;
        floating_point y = ty-(int)ty;
        //std::cout << x << " " << y << "\n";
        floating_point n00 = nucleoid_density[(int)tx][(int)ty];
        floating_point n10 = nucleoid_density[(int)tx+1][(int)ty];
        floating_point n01 = nucleoid_density[(int)tx][(int)ty+1];
        floating_point n11 = nucleoid_density[(int)tx+1][(int)ty+1];
        //std::cout << n00 << " " << n10 << "\n";
        //std::cout << n01 << " " << n11 << "\n\n";
        floating_point dx = ((n10*(1-y)+n11*y)-(n00*(1-y)+n01*y))/nx*Lx;
        floating_point dy = ((n01*(1-x)+n11*x)-(n00*(1-x)+n10*x))/ny*Ly;
        Point d{dx,dy};
        //std::cout << d.x << " " << d.y << "\n\n\n\n";
        return d;
    }
};

//------------------------------------------------------------------------------------------PC------------------------------------------------------------------------------------------
//One instance of this class handles one parition complex and all ParA contected to it
class PC
{
public:
    Domain& dom;

    //preallocate parameters
    floating_point dt;
    floating_point D_P;// = 0.003; //diffusion partition complex
    floating_point R;// = 0.05 + 0.002; //
    floating_point RR;//=R*R;
    floating_point tau_off;// = 1/0.1;
    floating_point sigma_x;// = 0.1; //Standard deviations of fluctuating DNA-bound ParA
    floating_point sigma_y;// = 0.05;

    //preallocate variabless
    floating_point sd;
    floating_point kx;//in units of k_b T,    sigma is the variance of equilibrium distribution
    floating_point Kx; //this parameter is the slope of the velocity profile of particle in a harmonic potential
    floating_point ky;
    floating_point Ky;
    floating_point Sx;
    floating_point expx;
    floating_point sdx;
    floating_point Sy;
    floating_point expy;
    floating_point sdy;
    int n_pb;

    //dormant PCs cannot bind ParA
    bool dormant;

    Point pos;//position of the partition complex

    std::vector<Point> home_pos;
    std::vector<Point> curr_pos;//positions of bound ParA

    std::vector<floating_point> normrands;

    Reaction_List tether_breaking{tau_off};

    PC(Domain& dom, floating_point dt, floating_point D_P, int n_pb, floating_point tau_off, floating_point R, floating_point sigma_x, floating_point sigma_y, bool dormant):dom(dom),
        tau_off(tau_off),D_P(D_P),n_pb(n_pb),R(R),RR(R*R),dt(dt),sigma_x(sigma_x),sigma_y(sigma_y),dormant(dormant),
        sd(sqrt(2*D_P*dt)),
        normrands(2)
    {
        pos.x = dom.Lx/5;
        pos.y = dom.Ly/2;
    };

    void offset_x_due_to_growth(floating_point growth_factor)
    {
        pos.x*=growth_factor;
        for(size_t i=0; i<home_pos.size(); i++)
        {
            home_pos[i].x*=growth_factor;
            curr_pos[i].x*=growth_factor;
        }
    }

    //returns if a point is in binding proximity
    bool binds(Point p)
    {
        if(n_pb>curr_pos.size())
        {
            floating_point x_distance = std::abs(p.x-pos.x);
            if(x_distance<R)
            {
                floating_point y_distance = std::abs(p.y-pos.y);
                if(y_distance<R)
                {
                    return(y_distance*y_distance+x_distance*x_distance<RR&&!dormant);
                }
            }
        }
        return false;
    }

    //carries out each reaction that occured (ParA which is released from the partition complex) in the last time step and returns how many are released.
    //this number is important because they are added to the cyto pool of ParA
    int tethers_breaking(floating_point t)
    {
        int index = -1;
        int n_broken = 0;
        while((index = tether_breaking.did_a_reaction_occur(t)) != -1)
        {
            n_broken++;
            remove_ParA(index);
        }
        return n_broken;
    }

    //move the partition complex and bound ParA based on the ParA which are connected to it
    void complex_move()
    {
        //number of springs/ParA attached to partition complex
        Random_Number_Engine::random_norm_vec(normrands);
        floating_point n = curr_pos.size();

        Point home_pos_pc;
        Point spring_vec;
        Point d;
        Point next_pos;
        if(curr_pos.size()>0)//if atleast one ParA is bound do an elastic move
        {
            for(auto i = 0; i < curr_pos.size(); i++)
            {
                spring_vec += home_pos[i]-curr_pos[i];
            }
            home_pos_pc.x = spring_vec.x/curr_pos.size()+pos.x;
            home_pos_pc.y = spring_vec.y/curr_pos.size()+pos.y;

            //calculate elastic move based on n spring
            kx = 1/pow(sigma_x,2) * n;//n;
            //kx = 1/sigma_x * n;//n;
            Kx = D_P*kx;
            ky = 1/pow(sigma_y,2) * n;//n;
            //ky = 1/sigma_y * n;//n;
            Ky = D_P*ky;
            Sx = 1-exp(-2*dt*Kx);
            expx = exp(-dt*Kx);
            sdx = sqrt(Sx/kx);
            //sdx = Sx/kx;
            Sy = 1-exp(-2*dt*Ky);
            expy = exp(-dt*Ky);
            sdy = sqrt(Sy/ky);
            //sdy = Sy/ky;

            d.x = (pos.x-home_pos_pc.x)*expx+sdx*normrands[0];
            d.y = (pos.y-home_pos_pc.y)*expy+sdy*normrands[1];
            next_pos = d+home_pos_pc;




        }
        else//otherwise do normal diffusion
        {
            d.x=sd*normrands[0];
            d.y=sd*normrands[1];
            next_pos = d+pos;
        }

        //update own position and position of all bound ParA
        dom.reflective_border(next_pos,R);
        d = next_pos-pos;
        curr_pos += d;
        pos = next_pos;
    }

    //add a ParA to the lists (position and reaction) of bound ParA
    void add_ParA(Point h, Point c, floating_point t)
    {
        tether_breaking.add_reaction(home_pos.size(),t);
        home_pos.push_back(h);
        curr_pos.push_back(c);
    }

    //removes a ParA from the lists (position and reaction) of bound ParA
    void remove_ParA(int index)
    {
        tether_breaking.remove_reaction(index);
        home_pos.erase(home_pos.begin()+index);
        curr_pos.erase(curr_pos.begin()+index);
    }

    Point get_force_vector()
    {
        Point fv;
        for(int i = 0; i < home_pos.size(); i++)
        {
            fv = fv + (home_pos[i]-curr_pos[i]);
        }
        return fv;
    }

    Point get_binding_vector()
    {
        Point bv;
        for(int i = 0; i < home_pos.size(); i++)
        {
            bv = bv + (curr_pos[i]-pos);
        }
        return bv;
    }

    Point asymmerty_ParA(floating_point nc)
    {
        Point res={0,0};
        for(int i = curr_pos.size()-1; i >= 0; i--)
        {
            if(curr_pos[i].x<nc){
                res.x++;
            } else {
                res.y++;
            }
        }
        return res;
    }
};



//------------------------------------------------------------------------------------------ParA------------------------------------------------------------------------------------------
//One instance of this class handles all ParA which are only bound to the nucleoid or in the cytosol
//ParA bound to a partition complex are handled by the correspoding PC-instance
class ParA
{
public:
    Domain& dom;

    floating_point dt;
    floating_point D_A;// = 0.01; //diffusion ParA
    floating_point D_H;// = 0.0; //diffusion home position on nucleoid
    floating_point tau_a;// = 1/0.1; //nucleoid binding rate
    floating_point tau_d;// = 1/0.01; //nucleoid unbinding rate
    floating_point tau_c0;// = 1/0.01; //nucleoid cooperation between ParA binding rate
    floating_point sigma_x;// = 0.1; //Standard deviations of fluctuating DNA-bound ParA
    floating_point sigma_y;// = 0.05;
    floating_point p_n;// nucleoid potential

    //derived parameters
    floating_point kx;// = 1/pow(sigma_x,2);//in units of k_b T,    sigma is the variance of equilibrium distribution
    floating_point ky;// = 1/pow(sigma_y,2);
    floating_point Kx;//=D_A*kx; //this parameter is the slope of the velocity profile of particle in a harmonic potential
    floating_point Ky;//=D_A*ky;

    //diffusion
    floating_point sd;

    //elastic fluctuations
    floating_point Sx;
    floating_point expx;
    floating_point sdx;

    floating_point Sy;
    floating_point expy;
    floating_point sdy;

    //square root of var for creating correlated random numbers for ParA moving in a potential based on nucleoid
    floating_point sVar1;
    floating_point sVar2;
    floating_point corr;

    //length threshold
    floating_point length_threshold = 0.2;
    floating_point length_threshold_squared = length_threshold*length_threshold;

    //list which stores when which ParA will unbind
    Reaction_List nuc_unbinding{tau_d};//no clue why I cannot use the () here and have to use the {}
    Reaction_List nuc_binding{tau_a};
    Reaction_List nuc_coop_binding{tau_c0};

    //position lists, one entry for each ParA bound to the nucleoid
    std::vector<Point> home_pos;
    std::vector<Point> curr_pos;

    //preallocated vectors to randomly generated numbers
    std::vector<floating_point> normrands;
    std::uniform_real_distribution<floating_point> unidist;



    ParA(int n_parA, Domain& dom, floating_point dt, floating_point D_A, floating_point D_H, floating_point tau_a, floating_point tau_d, floating_point tau_c0,floating_point sigma_x, floating_point sigma_y,floating_point p_n):dom(dom),dt(dt),
        D_A(D_A), D_H(D_H), tau_a(tau_a), tau_d(tau_d), tau_c0(tau_c0), sigma_x(sigma_x), sigma_y(sigma_y),p_n(p_n),
        kx(1/pow(sigma_x,2)), ky(1/pow(sigma_y,2)), Kx(D_A*kx), Ky(D_A*ky),
        sd(sqrt(2*D_H*dt)),
        Sx(1-exp(-2*dt*Kx)),expx(exp(-dt*Kx)),sdx(sqrt(Sx/kx)),
        Sy(1-exp(-2*dt*Ky)),expy(exp(-dt*Ky)),sdy(sqrt(Sy/ky)),
        sVar1(sqrt(2*D_H*dt)), sVar2(sqrt(2/3.0*D_H*dt)*dt),corr(D_H*dt*dt),
        home_pos(n_parA),curr_pos(n_parA),
		normrands(4*n_parA)
    {

        //each ParA starts at a random position
        for(auto i=0; i<n_parA; i++)
        {
		    Point hp = dom.get_binding_position();
		    Point cp{hp.x,hp.y};
            home_pos[i]=hp;
		    curr_pos[i]=cp;
            nuc_unbinding.add_reaction(i,0);
        }
    };

    void offset_x_due_to_growth(floating_point growth_factor)
    {
        for(size_t i=0; i<home_pos.size(); i++)
        {
            home_pos[i].x*=growth_factor;
            curr_pos[i].x*=growth_factor;
        }
    }

    //carries out all reactions (nuc-bound-ParA released to the cytosol and cytosol-ParA binding the nucleoid) which happend in the last time step
    int nuc_binding_unbinding_ParA(floating_point t)
    {
        floating_point tau_c = tau_c0/nuc_unbinding.reactions.size();
        nuc_coop_binding.tau = tau_c;
        // ParA binding the nucleoid du to coop
        int index = -1;
        int n_nuc = home_pos.size();
        floating_point nuc_index = -1;
        while((index = nuc_coop_binding.did_a_reaction_occur(t)) != -1)//cytosol-ParA binding the nucleoid cooperation
        {

            if(dom.has_nucleoid_density){
                std::vector<floating_point> unirands2(3);
                while(true) {
                    std::vector<floating_point> unirands(2);
                    Random_Number_Engine::random_uni_vec(unirands);
                    nuc_index = floor(unirands[0]*n_nuc);
                    Point p = home_pos[nuc_index];
                    floating_point density = dom.get_density(p);
                    if(unirands[1]<density){
                        break;
                    }
                }
            } else {
                std::vector<floating_point> unirands(1);
                Random_Number_Engine::random_uni_vec(unirands);
                nuc_index = floor(unirands[0]*n_nuc);
            }

            transition_cyto2nuc_ParA(t,index,home_pos[nuc_index],curr_pos[nuc_index]);
        }


        // ParA binding the nucleoid
        index = -1;
        while((index = nuc_binding.did_a_reaction_occur(t)) != -1)//cytosol-ParA binding the nucleoid normal
        {
            //std::cout << "nuc\n";
            transition_cyto2nuc_ParA(t,index);
        }



        // ParA unbinding the nucleoid due to basal hydro
        int n_nuc_unbinding_events = 0;
        index = -1;
        while((index = nuc_unbinding.did_a_reaction_occur(t)) != -1)//nuc-bound-ParA released to the cytosol
        {
            transition_nuc2cyto_ParA(t,index);
            n_nuc_unbinding_events++;
        }

        //std::cout << nuc_binding.reactions.size() << "\n";
        //std::cout << nuc_coop_binding.reactions.size() << "\n";
        //std::cout << "\n";
        return n_nuc_unbinding_events;
    }

    //spawns a new ParA to the nucleoid at a random position and appends to current lists (position reaction) with a random position
    void transition_cyto2nuc_ParA(floating_point t, int index_cyt)
    {
        //std::vector<floating_point> unirands(2);
        //Random_Number_Engine::random_uni_vec(unirands);
        //Point p{dom.Lx*unirands[0],dom.Ly*unirands[1]};
        Point hp = dom.get_binding_position();
        Point cp{hp.x,hp.y};
        transition_cyto2nuc_ParA(t,index_cyt,hp,cp);
    }

    //spawns a new ParA to the nucleoid at a given position and appends to current lists (position reaction) with a given position
    void transition_cyto2nuc_ParA(floating_point t, int index_cyt, Point hp, Point cp)
    {
        nuc_binding.remove_reaction(index_cyt);
        nuc_coop_binding.remove_reaction(index_cyt);
        int index_nuc = nuc_unbinding.reactions.size();
        nuc_unbinding.add_reaction(index_nuc,t);
        home_pos.push_back(hp);
        curr_pos.push_back(cp);
    }

    //remove a ParA from all lists (position reaction) and add it back to the cyto pool
    void transition_nuc2cyto_ParA(floating_point t,int index)
    {
        nuc_binding.add_reaction(nuc_binding.reactions.size(),t);
        nuc_coop_binding.add_reaction(nuc_coop_binding.reactions.size(),t);
        home_pos.erase(home_pos.begin()+index);
        curr_pos.erase(curr_pos.begin()+index);
        nuc_unbinding.remove_reaction(index);
    }

    void transition_nuc2plas_ParA(int index)
    {
        home_pos.erase(home_pos.begin()+index);
        curr_pos.erase(curr_pos.begin()+index);
        nuc_unbinding.remove_reaction(index);
    }

    //add n-many ParA to the cytosol-pool (this function is used to add ParA back to the pool which were bound by the plasmid
    void add_cyto_ParAs(int n,int t)
    {
        for(size_t i = 0; i < n; i++)
        {
            nuc_binding.add_reaction(nuc_binding.reactions.size(),t);
            nuc_coop_binding.add_reaction(nuc_coop_binding.reactions.size(),t);
        }
    }

    //checks which ParA bind to a given partition complex and removes them from all lists (position reaction)
    void binding_events(std::unique_ptr<PC> & pc,floating_point t)
    {
        for(int i = home_pos.size()-1; i >= 0; i--)
        {
            if(pc->binds(curr_pos[i]))
            {
                pc->add_ParA(home_pos[i],curr_pos[i],t);
                transition_nuc2plas_ParA(i);
            }
        }
    }

    Point asymmerty_ParA(floating_point nc)
    {
        Point res={0,0};
        for(int i = curr_pos.size()-1; i >= 0; i--)
        {
            if(curr_pos[i].x<nc){
                res.x++;
            } else {
                res.y++;
            }
        }
        return res;
    }

    //changes the position of all ParA bound only to the nucleoid.
    //standart diffusion is applied to the home position and elastic diffusion is applied to the current position
    void diffusive_and_elastic_move()
    {
        normrands.resize(6*home_pos.size());
        Random_Number_Engine::random_norm_vec(normrands);

        Point d1;
        Point d2;
        Point new_pos;
        Point old_pos;
        for(size_t i=0; i<home_pos.size(); i++)
        {
            //diffusive
            if(D_H>0)
            {
                if(dom.has_nucleoid_density)
                {

                    old_pos=home_pos[i];
                    d1 = dom.get_derivative(old_pos);
                    d1 = move_in_field(d1, normrands[2*i], normrands[2*i+1], normrands[2*i+2], normrands[2*i+3]);
                }
                else
                {
                    d1={sd*normrands[2*i],sd*normrands[2*i+1]};
                }
                new_pos=home_pos[i]+d1;
                dom.reflective_border(new_pos);
                home_pos[i]=new_pos;

            }


            //elastic
            new_pos = home_pos[i];//set new_pos to home_pos (I am reusing new_pos so I dont need to declare another variable)
            old_pos=curr_pos[i];
            d2={(old_pos.x-new_pos.x)*expx+sdx*normrands[2*i+4], (old_pos.y-new_pos.y)*expy+sdy*normrands[2*i+5]};
            new_pos={new_pos+d1+d2};
            dom.reflective_border(new_pos);

            curr_pos[i]=new_pos;
        }
    }

    Point move_in_field(Point f, floating_point norm1, floating_point norm2, floating_point norm3, floating_point norm4)
    {
        Point r1 = corr_gau(norm1, norm2);
        Point r2 = corr_gau(norm3, norm4);
        Point r3{r1.x,r2.x};
        Point r4{r1.y,r2.y};
        Point term2 = r3;
        Point term3 = D_H * p_n * f * dt;
        Point term4 = D_H*D_H / 2 * p_n*p_n * f * dt*dt;
        Point term5 = D_H * p_n * r4;
        //std::cout << r1.x << " " << r1.y << "\n";
        return  term2 + term3 + term4 + term5;
    }

    Point corr_gau(floating_point norm1, floating_point norm2) //correlated random numbers
    {
        //std::cout << sVar1 << " " << sVar2 << "\n";
        floating_point phi = corr/(sVar1*sVar2);
        phi = 1/2*asin(phi);
        floating_point a = cos(phi)*sVar1;
        floating_point b = sin(phi)*sVar1;
        floating_point c = sin(phi)*sVar2;
        floating_point d = cos(phi)*sVar2;
        Point r(a*norm1+b*norm2,c*norm1+d*norm2);
        //std::cout << r.x << " " << r.y << "\n";
        return r;
    }
};

